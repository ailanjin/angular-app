import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAccountBoardComponent } from './my-account-board.component';

describe('MyAccountBoardComponent', () => {
  let component: MyAccountBoardComponent;
  let fixture: ComponentFixture<MyAccountBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAccountBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAccountBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
