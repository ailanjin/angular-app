import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-account-board',
  templateUrl: './my-account-board.component.html',
  styleUrls: ['./my-account-board.component.css']
})
export class MyAccountBoardComponent implements OnInit {

  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  
  constructor() { }

  ngOnInit() {
  }

}
